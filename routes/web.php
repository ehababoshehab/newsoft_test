<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/mionion/{input}', 'MinionController@fix');

Route::get('/', function () {return view('welcome');});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');



Route::middleware(['auth', 'is_admin'])->group(function() {
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/users', 'UsersController@index')->name('users');
  Route::get('/user/create', 'UsersController@create')->name('user.create');
  Route::post('/user/store', 'UsersController@store')->name('user.store');
  Route::get('/user/{id}/edit', 'UsersController@edit')->name('user.edit');
  Route::post('/user/{id}/update', 'UsersController@update')->name('user.update');
  Route::get('/user/{id}/destroy', 'UsersController@destroy')->name('user.destroy');

  Route::get('/listings', 'ListingsController@index')->name('listings');
  Route::get('/list/create', 'ListingsController@create')->name('list.create');
  Route::post('/list/store', 'ListingsController@store')->name('list.store');
  Route::get('/list/{id}/edit', 'ListingsController@edit')->name('list.edit');
  Route::post('/list/{id}/update', 'ListingsController@update')->name('list.update');
  Route::get('/list/{id}/destroy', 'ListingsController@destroy')->name('list.destroy');
});
