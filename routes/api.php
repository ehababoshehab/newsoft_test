<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api', 'is_user'])->group(function() {
  Route::post('/login', 'Api\AuthController@login');
});

Route::middleware(['auth:api', 'check_access_token'])->group(function() {
  Route::get('/user', 'Api\UserController@user');
  Route::get('/listing', 'Api\UserController@listing');
});
