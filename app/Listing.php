<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
  protected $table = "listing";

  public $timestamps = false;

  protected $fillable = [
      'list_name', 'address', 'longitude', 'latitude', 'submitter_id',
  ];

  public function user()
  {
  	return $this->belongsTo(User::class, 'submitter_id');
  }
}
