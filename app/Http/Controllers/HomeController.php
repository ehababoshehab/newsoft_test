<?php

namespace App\Http\Controllers;

use App\User;
use App\Listing;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $usersCount = User::count();
      $listingsCount = Listing::count();
      return view('home')->with([
        'usersCount'    => $usersCount,
        'listingsCount' => $listingsCount
      ]);
    }
}
