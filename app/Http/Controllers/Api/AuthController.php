<?php

namespace App\Http\Controllers\Api;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
  public function Login(Request $request)
  {

    $user = User::where('email', $request->email)->first();

    $checkPassword  = Hash::check($request->password, $user->password);
    if ($checkPassword) {
      $token = $user->createToken('Laravel Password Grant Client')->accessToken;
      $response = array(
        "user_id" => "$user->id",
        "token" => "$token",
        "status" => array("code" => 200, "message" => "Access Granted."));
      return response($response, 200);
    }
    $response = array(
      "user_id" => "$user->id",
      "status" => array("code" => 422, "Message" => "Password missmatch."));
    return response($response, 422);
  }

}
