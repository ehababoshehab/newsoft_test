<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
  public function user(Request $request)
  {
    $response = array("user" => $request->user(),
      "status" => array("code" => 200, "message" => "User information successfully retrieved."));
    return response($response, 200);
  }

  public function listing(Request $request)
  {
    $longFrom = $request->query('longitude');
    $latFrom = $request->query('latitude');

    $listing = $request->user()->listing;
    foreach ($listing as $list) {
      $list->distance = $this->distance($longFrom, $list->longitude, $latFrom, $list->latitude);
      $list->offsetUnset('longitude');
      $list->offsetUnset('latitude');
      $list->offsetUnset('submitter_id');
    }

    $response = array("listing" => $listing,
      "status" => array("code" => 200, "message" => "Listing successfully retrieved."));
    return response($response, 200);
  }

  private function distance($longFrom, $longTo, $latFrom, $latTo, $earthRadius = 6371)
  {
    $longFrom = deg2rad($longFrom);
    $longTo = deg2rad($longTo);

    $latFrom = deg2rad($latFrom);
    $latTo = deg2rad($latTo);

    $latDelta = $latTo - $latFrom;
    $longDelta = $longTo - $longFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
      cos($latFrom) * cos($latTo) * pow(sin($longDelta / 2), 2)));

    $distance =  $angle * $earthRadius;

    return number_format($distance,3);
  }
}
