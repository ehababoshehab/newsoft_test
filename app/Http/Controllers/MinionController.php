<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MinionController extends Controller
{
  public function fix($n)
  {
    $string = "";
    $maxString;
    if ($n == 0) {
      $maxString = 5;
    } else {
      $maxString = $n + 5;
    }
    for ($i=0; $i < $maxString ; $i++) {
      if ($this->isPrime($i)) {
        $string .= $i;
      } else {
        $maxString++;
      }
    }
    $rm = substr($string, $n);
    $finalResult = substr($rm, 0, 5);
    return $finalResult;
  }

  private function isPrime($number) {
    if ( $number == 1 ) {
        return false;
    }
    if ( $number == 2 ) {
        return true;
    }
    $x = sqrt($number);
    $x = floor($x);
    for ( $i = 2 ; $i <= $x ; ++$i ) {
        if ( $number % $i == 0 ) {
            break;
        }
    }

    if( $x == $i-1 ) {
        return true;
    } else {
        return false;
    }
  }
}
