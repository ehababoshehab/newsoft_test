<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

use App\User;

use Illuminate\Support\Facades\Validator;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $validator = Validator::make($request->all(), [
        'email' => 'required|email|max:255',
        'password' => 'required|string|min:6',
      ]);

      if ($validator->fails()) {
        if ($validator->errors()->first('email')) {
          $error = $validator->errors()->first('email');
        }elseif ($validator->errors()->first('password')) {
          $error = $validator->errors()->first('password');
        }
        $response = array("status" => array("code" => 422, "message" => $error));
        return response($response, 422);
      }

      $user = User::where('email', $request->email)->first();

      if ($user) {
        return $next($request);
      }
      $response = array(
        "status" => array("code" => 422, "Message" => "User is not exist."));
      return response($response, 422);
    }

}
