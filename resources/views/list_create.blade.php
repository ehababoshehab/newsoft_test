@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create new list</div>

                <div class="card-body">
                <form class="" action="{{route('list.store')}}" method="post">
                  @csrf
                  <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="list_name" value="{{old('list_name')}}" class="form-control @error('list_name') is-invalid @enderror" required>
                    @error('list_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" name="address" value="{{old('address')}}" class="form-control @error('address') is-invalid @enderror" required>
                    @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Longitude</label>
                    <input type="text" name="longitude" value="{{old('longitude')}}" class="form-control @error('longitude') is-invalid @enderror" required>
                    @error('longitude')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Latitude</label>
                    <input type="text" name="latitude" value="{{old('latitude')}}" class="form-control @error('latitude') is-invalid @enderror" required>
                    @error('latitude')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Create">
                  </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
