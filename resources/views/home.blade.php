@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Model</th>
                        <th scope="col">Count</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Users</td>
                        <td>{{$usersCount}}</td>
                        <td><a href="{{route('users')}}" class="btn btn-primary">Check</a></td>
                      </tr>
                      <tr>
                        <td>Listings</td>
                        <td>{{$listingsCount}}</td>
                        <td><a href="{{route('listings')}}" class="btn btn-primary">Check</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
