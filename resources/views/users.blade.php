@extends('layouts.app')
@section('style')
  <style media="screen">
    .mg_bt_10 {margin-bottom:10px;}
  </style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users</div>

                <div class="card-body">
                  <a href="{{route('user.create')}}" class="btn btn-primary mg_bt_10">Add new user</a>
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Type</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($users as $user)
                        <tr>
                          <td>{{$user->id}}</td>
                          <td>{{$user->name}}</td>
                          <td>{{$user->email}}</td>
                          <td>
                            @if ($user->type == \App\User::ADMIN)
                              ADMIN
                            @else
                              USER
                            @endif
                          </td>
                          <td>
                            <a href="{{route('user.edit', $user->id)}}" class="btn btn-success">Edit</a>
                            <a href="{{route('user.destroy', $user->id)}}" class="btn btn-danger">Delete</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
