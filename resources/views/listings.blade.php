@extends('layouts.app')
@section('style')
  <style media="screen">
    .mg_bt_10 {margin-bottom:10px;}
  </style>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Lists</div>

                <div class="card-body">
                  <a href="{{route('list.create')}}" class="btn btn-primary mg_bt_10">Add new list</a>
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Latitude</th>
                        <th scope="col">Longitude</th>
                        <th scope="col">Submitter</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($listings as $list)
                        <tr>
                          <td>{{$list->id}}</td>
                          <td>{{$list->list_name}}</td>
                          <td>{{$list->address}}</td>
                          <td>{{$list->latitude}}</td>
                          <td>{{$list->longitude}}</td>
                          <td>{{$list->user->name}}</td>
                          <td>
                            <a href="{{route('list.edit', $list->id)}}" class="btn btn-success">Edit</a>
                            <a href="{{route('list.destroy', $list->id)}}" class="btn btn-danger">Delete</a>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
