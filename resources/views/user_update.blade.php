@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create new user</div>

                <div class="card-body">
                <form class="" action="{{route('user.update', $user->id)}}" method="post">
                  @csrf
                  <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control @error('name') is-invalid @enderror" required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" name="email" value="{{$user->email}}" class="form-control @error('email') is-invalid @enderror" required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Name</label>
                    <select class="form-control @error('type') is-invalid @enderror" name="type" required>
                      <option value>- Select -</option>
                      <option @if($user->type == \App\User::ADMIN) selected @endif value="{{\App\User::ADMIN}}">ADMIN</option>
                      <option @if($user->type == \App\User::USER) selected @endif value="{{\App\User::USER}}">User</option>
                    </select>
                    @error('type')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" value="{{old('password')}}" class="form-control @error('password') is-invalid @enderror" placeholder="Leave it empty if you do not want to change it.">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="">Password confirmation</label>
                    <input type="password" name="password_confirmation" value="{{old('password_confirmation')}}" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Leave it empty if you do not want to change it.">
                    @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Upadte">
                  </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
