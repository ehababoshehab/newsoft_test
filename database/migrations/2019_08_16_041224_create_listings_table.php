<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('list_name', 80);
          $table->string('address', 255);
          $table->decimal('longitude', 10, 7);
          $table->decimal('latitude', 10, 7);
          $table->bigInteger('submitter_id')->unsigned();

          $table->foreign('submitter_id')->references('id')
          ->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
